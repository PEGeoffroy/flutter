import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class HomeButton extends StatelessWidget {

  HomeButton({
    Key key,
    this.title,
    this.pictureUrl
  }) : super(key: key);

  final String title;
  final String pictureUrl;


  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {print('You tap this GestureDetector component')},
      child: Container(
          decoration: new BoxDecoration(
            color: new Color.fromRGBO(255, 255, 255, 30),
            borderRadius: BorderRadius.circular(12.0),
          ),
          //color: Colors.yellow,
          width: 336,
          height: 160,
          margin: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SvgPicture.asset(
                this.pictureUrl,
                height: 47.48,
                width: 132.96,
                color: const Color.fromRGBO(64, 68, 145, 1.0),
              ),
              Container(
                margin: const EdgeInsets.only(top: 10.0),
                child :Text(
                    this.title.toUpperCase(),
                    style: TextStyle(
                      fontWeight: FontWeight.w900,
                      color: const Color.fromRGBO(64, 68, 145, 1.0),
                      fontStyle: FontStyle.italic,
                      fontSize: 22,)
                ),
              )
            ],
        )
      ),
    );
  }
}