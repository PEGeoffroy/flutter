import 'package:flutter/material.dart';
import '../widget/HomeButton.dart';
import '../widget/BottomAppButton.dart';

class Home extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/weather.png"),
            fit: BoxFit.cover,
          )
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                  'Hello World!',
                style: new TextStyle(
                  fontSize: 50.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                  'Choose your category',
                style: new TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                  fontStyle: FontStyle.italic,
                ),
              ),

              Container(
                margin: const EdgeInsets.only(top: 20.0),
                child : HomeButton(
                    title: 'Weather Forecast',
                    pictureUrl: 'assets/shades.svg'
                ),
              ),

              HomeButton(
                  title: 'History',
                  pictureUrl: 'assets/trump.svg'
              ),
            ],
          ),
        ),

      ),
        bottomNavigationBar: BottomAppBar(
          child: Container(
          height: 50,
          child: new Row(
            children: <Widget>[
              BottomAppButton(
                title: 'Weather'
              ),

              Container(
                height: 40,
                child : VerticalDivider(
                  width: 0,
                  thickness: 1.5,
                  color: Colors.grey,
                ),
              ),

              BottomAppButton(
                title: 'History'
              ),
            ],
          ),
          )
        )
    );
  }
}